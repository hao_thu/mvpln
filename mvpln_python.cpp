/*
  file: mvpln_python.cpp
  description: wrap the MVPLN model into a python model
  author: HW
 */

#include "mvpln.h"
#include <boost/python/stl_iterator.hpp>

/* python interface */
bp::list mvpln_predict(bp::list X, bp::list B){
	arma::mat arma_X = convert2mat(X);
	arma::mat arma_B = convert2mat(B);

	arma::mat arma_Y_est = predict_mvpln(arma_X, arma_B);

	bp::list Y_est = convert2pylist(arma_Y_est);

	return Y_est;
}

bp::dict mvpln_train(bp::list X, bp::list Y, bp::list B, bp::list Omega, 
		double lambda1, double lambda2, double epsilon, double gamma, 
		double tau, int N, bp::str dist_type = "f-mean", 
		bp::str prop_type = "N", int maxiter = 100){
	/* convert the data type */
	arma::mat arma_X = convert2mat(X), arma_Y = convert2mat(Y);
	arma::mat arma_B = convert2mat(B), arma_Omega = convert2mat(Omega);
	uint uN = uint(N), umaxiter = uint(maxiter);
	std::string prop_type_c = bp::extract<std::string>(prop_type);
	std::string dist_type_c = bp::extract<std::string>(dist_type);

	/* train the mvpln model */
	double y_max = arma::max(arma::vectorise(arma_Y));
	double_vec fact_table = prepare_log_factorial(y_max);
	model_param result = estimate_parameters(arma_X, arma_Y, arma_B, arma_Omega,
			lambda1, lambda2, epsilon, gamma, tau, uN, fact_table, dist_type_c,
			prop_type_c, umaxiter);

	/* summarize the result */
	bp::list py_B = convert2pylist(result.B);
	bp::list py_Omega = convert2pylist(result.Omega);
	bp::dict result_dict;
	result_dict["B"] = py_B, result_dict["Omega"] = py_Omega;
	result_dict["iter"] = result.niters, result_dict["lam1"] = result.lambda1;
	result_dict["lam2"] = result.lambda2, result_dict["eBIC"] = result.eBIC;
	result_dict["status"] = bp::str(result.status);

	return result_dict;
}

bp::dict mvpln_selection(bp::list X, bp::list Y, bp::list B, bp::list Omega, 
		bp::list lam1_vec, bp::list lam2_vec, double epsilon, double tau, 
		double gamma, int N, bp::str dist_type = "f-mean", 
		bp::str prop_type = "N", int maxiter = 100){
	/* convert the data type */
	arma::mat arma_X = convert2mat(X), arma_Y = convert2mat(Y);
	arma::mat arma_B = convert2mat(B), arma_Omega = convert2mat(Omega);
	bp::stl_input_iterator<double> lam1_begin(lam1_vec), lam1_end;
	bp::stl_input_iterator<double> lam2_begin(lam2_vec), lam2_end;
	double_vec lambda1_vec(lam1_begin, lam1_end);
	double_vec lambda2_vec(lam2_begin, lam2_end);
	std::string prop_type_c = bp::extract<std::string>(prop_type);
	std::string dist_type_c = bp::extract<std::string>(dist_type);
	uint uN = uint(N), umaxiter = uint(maxiter);

	/* select the best model with eBIC */
	double y_max = arma::max(arma::vectorise(arma_Y));
	double_vec fact_table = prepare_log_factorial(y_max);
	model_param best_model = model_selection(arma_X, arma_Y, arma_B, arma_Omega,
			lambda1_vec, lambda2_vec, epsilon, tau, gamma, uN, fact_table, 
			dist_type_c, prop_type_c, umaxiter);

	/* summarize the result */
	bp::list best_B = convert2pylist(best_model.B);
	bp::list best_Omega = convert2pylist(best_model.Omega);
	bp::dict result;
	result["B"] = best_B, result["Omega"] = best_Omega;
	result["iter"] = best_model.niters, result["eBIC"] = best_model.eBIC;
	result["lam1"] = best_model.lambda1, result["lam2"] = best_model.lambda2;
	result["status"] = bp::str(best_model.status);

	return result;
}

BOOST_PYTHON_MODULE(mvpln_cpp) {
	bp::def("mvpln_train", mvpln_train);
	bp::def("mvpln_predict", mvpln_predict);
	bp::def("mvpln_selection", mvpln_selection);
}
