/*
  file: mcem.cpp
  description: implement the MCEM algorithm
  author: HW
 */

#include "mcem.h"
#include <algorithm>
#include <iostream>
#include <omp.h>

#define _OPENMP_

/*
  name: estimate_Omega_matrix
  description: compute Omega matrix with glasso implemented with Fortran
  paramemters:
  		Phi - empirical covariance matrix
		lambda2 - penalty parameter
  return: 
  		arma::mat - estimated Omega matrix
 */
arma::mat estimate_Omega_matrix(arma::mat Phi, double lambda2, int ia, int is,
	   int itr, int ipen, double thr, int maxit) {
	/* create the input parameters for glasso procedure in Fortran */
	arma::mat lambda(Phi.n_rows, Phi.n_cols);
	lambda.fill(lambda2);
	double *ss = Phi.memptr(), *rho = lambda.memptr();
	int n = Phi.n_rows;

	/* create the output variables for glasso procedure */
	arma::mat Omega(Phi.n_rows, Phi.n_cols, arma::fill::zeros);
	arma::mat Sigma(Phi.n_rows, Phi.n_cols, arma::fill::zeros);
	double *ww = Omega.memptr(), *wwi = Sigma.memptr();
	int niter = 0, jerr = 0;
	double del = 0.0;

	/* call the glasso in Fortran to estimate the Omega matrix */
	glasso(&n, ss, rho, &ia, &is, &itr, &ipen, &thr, &maxit, ww, wwi, &niter, 
			&del, &jerr);

	return Omega;
}

/*
  name: estimate_B_matrix_approx
  description: estimate the coefficient matrix B with quadratic approximation 
  			   of the L1 penalty term
  paramemters:
  		X - training data matrix
		B - current estimate of B matrix
		Omega - current estimate of Omega matrix
		Theta - vector of MH samples
		lambda1 - regularization parameters for B matrix
  return: 
  		arma::mat - estimated B matrix for next MCEM iterations
 */
arma::mat estimate_B_matrix_approx(arma::mat X, arma::mat B, arma::mat Omega,
		mat_vec Theta, double lambda1){
	uint m = Theta[0].n_rows, n = X.n_rows, p = X.n_cols, q = B.n_cols;
	double small_value = 1.0e-20;

	/* compute the estimation of B matrix */
	arma::mat S(p, p, arma::fill::zeros), H_temp(p, q, arma::fill::zeros);
	for (uint i = 0; i < n; ++i) {
		/* construct X_i matrix */
		arma::mat X_i = arma::repmat(X.row(i), m, 1);
		S = S + X_i.t() * X_i;
		H_temp = H_temp + X_i.t() * arma::log(Theta[i]);
	} /* end of loop for(i) */
	arma::mat H = H_temp * Omega;
	B.elem(arma::find(B > -small_value && B < small_value)).fill(small_value);
	arma::mat penalty = lambda1 * m * n / arma::abs(B);
	arma::vec vec_B = arma::inv(arma::kron(Omega, S) + 
			arma::diagmat(arma::vectorise(penalty))) * arma::vectorise(H);
	arma::mat B_est(vec_B.memptr(), p, q);

	return B_est;
}

/*
  name: mc_e_step
  description: perform the MC E-step (mainly MH sampling for each training data)
  paramemters:
  		X, Y - training data matrix
		B, Omega - current estimate of model parameters
		N - number of MH samples for each data point
		fact_table - factorial table in log scale
		prop_type - proposal density type
		cov, tau - parameters for proposal density.
  return:
  		mat_vec - a vector of MH samples for each data point.
 */
mat_vec mc_e_step(arma::mat X, arma::mat Y, arma::mat B, arma::mat Omega, 
				  uint N, double_vec &fact_table, std::string prop_type, 
				  double tau){
	arma::mat Sigma = Omega.i();
	std::vector< std::pair<uint, arma::mat> > aux_vec;
	uint n = X.n_rows;

#ifdef _OPENMP_
#pragma omp parallel for schedule(dynamic, 1) shared(aux_vec)
#endif
	for (uint i = 0; i < n; ++i) {
		/* compute theta_hat and covariance matrix */
		arma::vec curr_x = arma::vectorise(X.row(i));
		arma::vec curr_y = arma::vectorise(Y.row(i));
		vec_mat_pair mode_cov = find_mode_cov_approx(curr_y, curr_x, curr_y, B,
				Sigma, tau);
		/* MH sampling */
		arma::mat Theta_i = MH_sampling(mode_cov.first, curr_x, curr_y, B, 
				Sigma, N, fact_table, prop_type, mode_cov.second);
#ifdef _OPENMP_
#pragma omp critical
#endif
		{
			aux_vec.push_back(std::make_pair(i, Theta_i));
		}
	} /* end of loop for(i) */

	/* reorder the MH samples according to the indices */
	struct pair_less_than{
		bool operator() (const std::pair<uint, arma::mat> &lhs, 
				const std::pair<uint, arma::mat> &rhs) {
			return (lhs.first < rhs.first);
		}
	} comparator;
	std::sort(aux_vec.begin(), aux_vec.end(), comparator);
	mat_vec result;
	for (uint i = 0; i < n; ++i) {
		result.push_back(aux_vec[i].second);
	} /* end of loop for(i) */

	return result;
}

/*
  name: m_step
  description: M-step of MCEM algorithm, maximize the expected log-likelihood.
  paramemters:
  		X - training data matrix
		B, Omega - current estimate of model parameters
		Theta - vector of MH samples
		lambda1, lambda2 - regularization parameters for B and Omega
		epsilon - stop criterion threshold
		maxiter - maximum allowed iterations, default is 100.
  return: 
  		The pair of estimated B and Omega
 */
mat_pair m_step(arma::mat X, arma::mat B, arma::mat Omega, mat_vec Theta, 
				double lambda1, double lambda2, double epsilon, 
				std::string dist_type, uint maxiter){
	/* initialization */
	arma::mat B_prev = B, Omega_prev = Omega;
	arma::mat B_curr, Omega_curr;
	uint n = X.n_rows, m = Theta[0].n_rows, iters = 0;

	while (true) {
		iters += 1;
		/* compute Phi matrix */
		arma::mat Phi = compute_Phi_mat(X, B_prev, Theta);

		/* call glasso to estimate Omega matrix */
		arma::mat S = Phi.t() * Phi / double(m * n);
		Omega_curr = estimate_Omega_matrix(S, lambda2);

		/* compute the estimate for B matrix */
		B_curr = estimate_B_matrix_approx(X, B_prev, Omega_curr, 
				Theta, lambda1);

		/* check the stop criterion */
		double Omega_stop = matrix_distance(Omega_curr, Omega_prev, dist_type);
		double B_stop = matrix_distance(B_curr, B_prev, dist_type);
		if (iters == maxiter || (Omega_stop < epsilon && B_stop < epsilon)) {
			break;
		}

		/* prepare for next iteration */
		B_prev = B_curr;
		Omega_prev = Omega_curr;
	} /* end of loop while */

	return std::make_pair(B_curr, Omega_curr);
}
