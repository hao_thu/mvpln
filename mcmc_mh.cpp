/*
  file: mcmc_mh.cpp
  description: implement the MCMC Metropolis sampling procedure
  author: HW
 */

#include "mcmc_mh.h"
#include <iostream>
#include <random>
#include <algorithm>
#include <stdexcept>
#include <exception>

/*
  name: multivariate_normal_distribution
  description: generate multivariate normal samples with given mu and 
  			   covariance matrix
  paramemters:
  		mu - location parameter
		cov - covariance matrix
  return:
  		arma::vec - generated sample
 */
arma::vec multivariate_normal_distribution (arma::vec mu, arma::mat cov){
	/* cholesky decomposition of the cov matrix */
	arma::mat R;
	try{
		R = arma::chol(cov, "lower");
	}
	catch(std::runtime_error e){
		std::cout << "error occurred: " << e.what() << std::endl;
		std::cout << "cov matrix: " << std::endl << cov << std::endl;
		arma::vec eig_vals = arma::conv_to<arma::vec>::from(arma::eig_gen(cov));
		std::cout << "eigen values of the cov matrix: " << std::endl << eig_vals
			<< std::endl;
		std::cout << (-1.1841e-17 < 0) << std::endl;
		std::cout << "R matrix: " << std::endl << R << std::endl;
		exit(1);
	}

	/* generate independent multivariate standard normal */
	arma::vec z = arma::randn<arma::vec>(mu.n_elem);
	arma::vec result = mu + R * z;

	return result;
}

/*
  name: r_proposal_distribution
  description: propose a candidate from the specified proposal distribution
  paramemters:
  		N - number of proposed candidates
		vec_loc, cov - location & covariance matrix parameter for proposal 
					   distribution.
		prop_type - type of proposal distribution. Currently available options:
					"N" - multivariate normal distribution
  return: 
  		arma::vec - proposed candidate
 */
arma::vec r_proposal_distribution(arma::vec vec_loc, arma::mat cov, 
		std::string prop_type){
	arma::vec vec_cand;

	while (true) {	
		if (prop_type == "N") {
			vec_cand = multivariate_normal_distribution(vec_loc, cov);
		}

		/* check whether the proposal is all positive */
		if (arma::all(vec_cand > 0))
			break;
	} /* end of loop while */

	return vec_cand;
}

/*
  name: proposal_density_log
  description: compute the value of proposal density function at given 
  			   location in log scale.
  paramemters: 
  		vec_x - the given location
		vec_loc, cov - location and covariance matrix for proposal density
		prop_type - type of proposal distribution
  return:
  		double - density value in log scale
 */
double proposal_density_log(arma::vec vec_x, arma::vec vec_loc, 
		std::string prop_type, arma::mat cov){
	double density_value = 0;

	if (prop_type == "N") {
		/* symmetric density, no need to compute due to the cancelation */
		density_value = 0;
	}

	return density_value;
}

/*
  name: target_density_log
  description: compute the value of target density function at given point in 
  			   log scale.
  paramemters: 
  		vec_theta - the given point to evaluate
		vec_x, vec_y - training data point x & y
		B, Sigma - current estimate of B and Sigma matrix
		fact_table - factorial table in log scale
  return:
  		double - value of target density in log scale
 */
double target_density_log(arma::vec vec_theta, arma::vec vec_x, 
						  arma::vec vec_y, arma::mat B, arma::mat Sigma, 
						  double_vec &fact_table) {
	uint dim = vec_theta.n_elem;
	/* check whether there is zero in vec_theta */
	if (!arma::all(vec_theta > 0)) {
		vec_theta.rows(arma::find(vec_theta == 0)).fill(1.0e-20);
	}

	/* compute the Poisson part in the target density in log scale */
	double poisson_part = arma::sum(vec_y % arma::log(vec_theta) - vec_theta - 
			log_factorial(vec_y, fact_table));

	/* compute the lognormal part in the density function */
	arma::mat Omega = Sigma.i();
	arma::vec temp_result = (arma::log(vec_theta) - B.t() * vec_x).t() * Omega 
		* (arma::log(vec_theta) - B.t() * vec_x);
	double numerator = -0.5 * temp_result(0);
	double denominator = double(dim) / 2 * log(2 * arma::datum::pi) + 
			0.5 * log(arma::det(Sigma)) + arma::sum(arma::log(vec_theta));

	return poisson_part + numerator - denominator;
}

/*
  name: compute_hessian
  description: compute the hessian matrix of the target density function at the given location init_theta.
  paramemters:
  		init_theta - the given location for hessian matrix
		vec_x, vec_y - associated training data point
		B, Simga - current estimate of model parameters
  return: 
  		arma::mat - hessian matrix
 */
arma::mat compute_hessian(arma::vec vec_theta, arma::vec vec_x, 
		arma::vec vec_y, arma::mat B, arma::mat Sigma){
	arma::mat theta_inv = arma::diagmat(1.0 / vec_theta);
	arma::mat Omega = Sigma.i();
	arma::vec vec_temp = Omega * (arma::log(vec_theta) - B.t() * vec_x);
	arma::mat hessian = theta_inv * Omega * theta_inv - 
		arma::pow(theta_inv, 2) * arma::diagmat(vec_y - 1) - 
		arma::pow(theta_inv, 2) * arma::diagmat(vec_temp);

	return hessian;
}

/*
  name: find_mode_cov_approx
  description: compute the location of the mode of the target density function 
  			   and corresponding hessian matrix with first order Taylor 
			   expansion.
  paramemters: 
  		init_theta - the initial theta to begin with.
		vec_x, vec_y - associated training data x & y
		B, Sigma - current estimate of model parameters.
		tau - tuning parameters for the estimated covariance matrix from hessian
  return:
  		vec_mat_pair - the location where the mode is and the corresponding 
					   estimated covariance matrix.
 */
vec_mat_pair find_mode_cov_approx (arma::vec init_theta, arma::vec vec_x, 
								   arma::vec vec_y, arma::mat B, 
								   arma::mat Sigma, double tau){
	/* compute the theta_hat */
	init_theta.rows(arma::find(init_theta == 0)).fill(1.0e-20);
	arma::vec init_kappa = arma::log(init_theta);
	arma::mat Omega = Sigma.i();
	arma::mat mat_first = arma::diagmat(init_theta) + Omega;
	arma::vec vec_second = vec_y - 1 + Omega * B.t() * vec_x + 
		arma::diagmat(init_theta) * init_kappa - init_theta;
	arma::vec kappa_hat;
	try{
		kappa_hat = mat_first.i() * vec_second;
	}
	catch(std::runtime_error &e){
		std::cerr << "kappa_hat problem: " << e.what() << std::endl << "matrix:" 
			<< std::endl << mat_first << std::endl << "determinant: " <<
			arma::det(mat_first) << std::endl;
		exit(1);
	}
	arma::vec mode_theta = arma::exp(kappa_hat);

	/* compute the estimated covariance matrix */
	mode_theta.rows(arma::find(mode_theta == 0)).fill(1.0e-20);
	arma::mat mat_hessian = compute_hessian(mode_theta, vec_x, vec_y, B, Sigma);
	arma::mat cov_est;
	try{
		cov_est = tau * arma::inv(-mat_hessian);
	}
	catch(std::runtime_error &e){
		arma::mat temp = -mat_hessian;
		std::cerr << "hessian matrix problem: " << e.what() << std::endl <<
			"matrix: " << temp << std::endl << "determinant: " <<
			arma::det(temp) << std::endl;
		exit(1);
	}

	/* check the PSD of the estimated covariance matrix */
	arma::vec eigen_values = arma::conv_to<arma::vec>::from(arma::eig_gen(cov_est));
	bool psd = arma::all(eigen_values >= 0);
	if (!psd) {
		/* check whether all the diagonal elements are negative */
		bool diag_negative = arma::all(arma::diagvec(cov_est) < 0);
		if(diag_negative){
			cov_est.diag() = arma::abs(cov_est.diag());
		}

		try{
			cov_est = nearPSD(cov_est).first;
		}
		catch (std::exception &e) {
			std::cout << e.what() << std::endl;
			std::cout << "covariance matrix after nearPSD: " << std::endl 
				<<cov_est << std::endl;
			exit(1);
		}
		/* further check the PSD property, the eigen value could not be too
		 * close to 0, otherwise, the cholesky decomposition may fail to 
		 * converge in the multivariate_normal_distribution method. */
		eigen_values = arma::conv_to<arma::vec>::from(arma::eig_gen(cov_est));
		double eig_min = arma::min(eigen_values);
		if (eig_min < 1.0e-10){
			cov_est = cov_est + (abs(eig_min) + 1.0e-10) * 
				arma::eye<arma::mat>(cov_est.n_rows, cov_est.n_cols);
		}
	}

	return std::make_pair(mode_theta, cov_est);
}

/*
  name: MH_sampling
  description: perform the Metropolis sampling to get N samples from target 
  			   distribution with OpenMP
  paramemters:
  		init_theta - start value for MH sampling
		vec_x, vec_y - the given training data point
		B, Sigma - current estimate of model parameters
		N - number of samples required
		fact_table - factorial table in log scale
		prop_type, cov - type of proposal distribution & covariance matrix for 
						 proposal distribution
  return:
  		arma::mat - N by q matrix, each row is a MH sample
 */
arma::mat MH_sampling(arma::vec init_theta, arma::vec vec_x, arma::vec vec_y,
					  arma::mat B, arma::mat Sigma, uint N, 
					  double_vec &fact_table, std::string prop_type, 
					  arma::mat cov) {
	arma::mat MH_samples(N, init_theta.n_elem);
	arma::vec theta_prev = init_theta;
	uint sample_num = 0;

	/* begin MH sampling */
	std::random_device device;
	std::mt19937 gen(device());
	while (sample_num < N) {
		/* propose a new theta */
		arma::vec theta_cand = r_proposal_distribution(theta_prev, cov, 
				prop_type);

		/* compute the acceptance ratio */
		double current_iter = target_density_log(theta_cand, vec_x, vec_y, B, 
				Sigma, fact_table) - proposal_density_log(theta_cand, 
					theta_prev, prop_type, cov);
		double last_iter = target_density_log(theta_prev, vec_x, vec_y, B, 
				Sigma, fact_table) - proposal_density_log(theta_prev, 
					theta_cand, prop_type, cov);
		double ratio = exp(current_iter - last_iter);
		double alpha = std::min(ratio, 1.0);

		/* decide whether to accept or reject the current proposal */
		std::bernoulli_distribution bernou(alpha);
		if (bernou(gen)) {
			MH_samples.row(sample_num) = theta_cand.t();
			theta_prev = theta_cand;
			sample_num += 1;
		}
	} /* end of loop while */

	/* discard the first 10% of the samples */
	int discard_num = int(N * 0.1);
	return MH_samples(arma::span(discard_num, N - 1), arma::span::all);
}
