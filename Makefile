CFLAGS= -g -O2 -Wall -Wextra -fopenmp -fPIC -std=c++0x
INCLUDE= -I /usr/include/python2.7/
LDFLAGS= -O2 -fopenmp -larmadillo -lgfortran -lboost_system -lboost_thread -lboost_log -lboost_log_setup
PYTHONFLAGS= -lpython2.7 -lboost_python
CC= g++
FORTRAN= gfortran

all: mvpln mvpln.so mvpln_real

mvpln_real: mvpln_realData.o mvpln.o mcmc_mh.o mcem.o utils.o logger.o glasso.o
	$(CC) -o mvpln_real mvpln_realData.o mvpln.o mcmc_mh.o mcem.o utils.o logger.o glasso.o $(PYTHONFLAGS) $(LDFLAGS)
mvpln: unit_test.o mvpln.o mcmc_mh.o mcem.o utils.o logger.o glasso.o
	$(CC) -o unit_test unit_test.o mvpln.o mcmc_mh.o mcem.o utils.o logger.o glasso.o $(PYTHONFLAGS) $(LDFLAGS)
mvpln.so: mvpln_python.o mvpln.o mcmc_mh.o mcem.o utils.o glasso.o
	$(CC) -shared -o mvpln.so mvpln_python.o mvpln.o mcmc_mh.o mcem.o utils.o logger.o glasso.o $(PYTHONFLAGS) $(LDFLAGS)

mvpln_realData.o: mvpln_realData.cpp mvpln.h mcmc_mh.h mcem.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c mvpln_realData.cpp
unit_test.o: unit_test.cpp mvpln.h mcmc_mh.h mcem.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c unit_test.cpp
mvpln_python.o: mvpln_python.cpp mvpln.h mcmc_mh.h mcem.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c mvpln_python.cpp
mvpln.o: mvpln.cpp mvpln.h mcmc_mh.h mcem.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c mvpln.cpp
mcem.o: mcem.cpp mcem.h mcmc_mh.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c mcem.cpp
mcmc_mh.o: mcmc_mh.cpp mcmc_mh.h utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c mcmc_mh.cpp
utils.o: utils.cpp utils.h logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c utils.cpp
logger.o: logger.cpp logger.h
	$(CC) $(CFLAGS) $(INCLUDE) -c logger.cpp
glasso.o: glasso.F
	$(FORTRAN) -fPIC -c glasso.F
	strip -N main glasso.o

clean:
	rm unit_test mvpln.so mvpln_model mvpln_real exp_tool exp_tool_real *.o
