/*
  file: main.cpp
  description: main file of the mvpln model
  author: HW
 */

#include "mvpln.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, const char *argv[])
{	
	/* 
	  load the data from csv file. Replace these file
	  names with your own data files.
	*/
	std::string B_file = "../r_script/B.csv";
	std::string Sigma_file = "../r_script/Sigma.csv";
	std::string X_file = "../r_script/X.csv";
	std::string Y_file = "../r_script/Y.csv";
	std::string X_test_file = "../r_script/X_test.csv";
	std::string Y_test_file = "../r_script/Y_test.csv";

	arma::mat B_true, Sigma_true, X, Y, X_test, Y_test;
	B_true.load(B_file, arma::csv_ascii);
	Sigma_true.load(Sigma_file, arma::csv_ascii);
	X.load(X_file, arma::csv_ascii);
	Y.load(Y_file, arma::csv_ascii);
	X_test.load(X_test_file, arma::csv_ascii);
	Y_test.load(Y_test_file, arma::csv_ascii);

	/* read the input parameters */
	if (argc != 7){
		std::cout << "Not enough parameters!" << std::endl;
		exit(1);
	}

	double lambda1 = atof(argv[1]), lambda2 = atof(argv[2]), 
		   epsilon = atof(argv[3]), gamma = atof(argv[4]), tau = atof(argv[5]);
	uint N = atoi(argv[6]);

	/* model initialization */
	uint y_max = arma::max(arma::vectorise(Y));
	double_vec fact_table = prepare_log_factorial(y_max);
	arma::mat Omega_true = Sigma_true.i();
	arma::mat B_init = arma::ones<arma::mat>(B_true.n_rows, B_true.n_cols) / 10.0;
	arma::mat Omega_init = arma::randn<arma::mat>(Omega_true.n_rows, 
			Omega_true.n_cols);
	Omega_init = Omega_init.t() * Omega_init;

	/* estimate the model */
	model_param result_model = estimate_parameters(X, Y, B_init, Omega_init, 
			lambda1, lambda2, epsilon, gamma, tau, N, fact_table, "rf-mean");

	/* write the model parameters (eBIC, lambda1, lambda2) into file */
	std::ofstream param_file;
	param_file.open("./intermediate_result/eBIC_lambda1_lambda2.csv", 
			std::ios::out | std::ios::app);
	param_file << result_model.eBIC << "," << result_model.lambda1 << ","
		<< result_model.lambda2 << std::endl;
	param_file.close();

	return 0;
}
