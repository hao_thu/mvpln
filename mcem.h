/*
  file: mcem.h
  description: delare the function for MCEM algorithm
  author: HW
 */

#ifndef MCEM_H
#define MCEM_H

#include <utility>
#include "mcmc_mh.h"

typedef std::pair<arma::mat, arma::mat> mat_pair;

/* 2d array in fortran is stored as flat column-major array */
/* glasso implemented in Fortran */
extern "C" void glasso(int *n, double *ss, double *rho, int *ia, int *is, 
					   int *itr, int *ipen, double *thr, int *maxit, 
					   double *ww, double *wwi, int *niter, double *del, 
					   int *jerr);

mat_vec mc_e_step(arma::mat X, arma::mat Y, arma::mat B, arma::mat Omega, 
				  uint N, double_vec &fact_table, std::string prop_type, 
				  double tau);

arma::mat estimate_Omega_matrix(arma::mat Phi, double lambda2, int ia = 0, 
								int is = 0, int itr = 0, int ipen = 1, 
								double thr = 1.0e-4, int maxit = 10000);

arma::mat estimate_B_matrix_approx(arma::mat X, arma::mat B, arma::mat Omega,
								   mat_vec Theta, double lambda1);

mat_pair m_step(arma::mat X, arma::mat B, arma::mat Omega, mat_vec Theta, 
				double lambda1, double lambda2, double epsilon, 
				std::string dist_type = "f-mean", uint maxiter = 100);

#endif
