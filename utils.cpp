/*
  file: utils.cpp
  description: implement the util functions
  author: HW
 */

#include "utils.h"
#include <assert.h>
#include <limits>
#include <iostream>
#include <random>
#include <fstream>

/*
  name: convert2mat
  description: convert the python list to armadillo matrix
  paramemters: 
  		py_mat - list format of python matrix
  return: 
  		armadillo matrix
 */
arma::mat convert2mat(bp::list py_mat){
	int nrow = bp::len(py_mat);
	int ncol = bp::len(py_mat[0]);
	arma::mat result_mat(nrow, ncol, arma::fill::zeros);

	for (int r = 0; r < nrow; ++r) {
		for (int c = 0; c < ncol; ++c) {
			result_mat(r,c) = bp::extract<double>(py_mat[r][c]);
		} /* end of loop for(c) */
	} /* end of loop for(r) */

	return result_mat;
}

/*
  name: convert2pylist
  description: convert the armadillo matrix to python list
  paramemters: 
  		arma_mat - armadillo matrix
  return: 
  		python list form of the matrix
 */
bp::list convert2pylist(arma::mat arma_mat){
	bp::list result_list;
	
	for (unsigned int r = 0; r < arma_mat.n_rows; ++r) {
		bp::list temp_row;
		for (unsigned int c = 0; c < arma_mat.n_cols; ++c) {
			temp_row.append(arma_mat(r,c));
		} /* end of loop for(c) */
		result_list.append(temp_row);
	} /* end of loop for(r) */

	return result_list;
}

/*
  name: multivariate_poisson
  description: generate multivariate poisson random variables, each dimension 
  			   is independent of each other.
  paramemters: 
  		mu - mean vector of the poisson distribution
		dim - dimension
  return:
  		arma::vec - genrated multivariate_poisson random variable
 */
arma::vec multivariate_poisson(arma::vec mu, uint dim){
	arma::vec result(dim, arma::fill::zeros);
	std::random_device device;
	std::mt19937 gen(device());

	for (uint i = 0; i < dim; ++i) {
		std::poisson_distribution<int> poisson_dist(mu(i));
		result(i) = poisson_dist(gen);
	} /* end of loop for(i) */

	return result;
}

/*
  name: matrix_norm
  description: compute the marix norm of specific type
  paramemters:
  		x - reference to the arma matrix
		norm_type - type of matrix norm to compute
					"F": matrix F Norm
					"I": matrix Infinity Norm
  return: 
  		double - specified type of matrix norm.
 */
double matrix_norm(arma::mat x, char norm_type) {
	double mat_norm = 0;

	if (norm_type == 'F') {			/* matrix F-Norm */
		mat_norm = arma::norm(x, "fro");
	}
	else if(norm_type == 'I'){
		arma::vec row_sum = arma::sum(arma::abs(x), 1);
		mat_norm = arma::max(row_sum);
	}

	return mat_norm;
}

/*
  name: matrix_distance
  description: compute the distance measure between two given matrix
  paramemters: 
  		mat_new - newly generated arma matrix reference
		mat_old - original arma matrix reference
		dist_type - type of distance measure
					"f-mean": matrix F-Norm dvided by matrix size
					"rf-mean": matrix F-Norm divided by matrix size where the 
							   division is inside the square root.
					"nf-norm": matrix F-Norm normalized by the F-Norm of mat_old
  return: double
 */
double matrix_distance(arma::mat mat_new, arma::mat mat_old, 
		std::string dist_type) {
	double result = 0;

	arma::mat mat_diff = mat_new - mat_old;
	double matrix_size = double(mat_diff.n_rows * mat_diff.n_cols);
	if (dist_type == "f-mean") {
		result = arma::norm(mat_diff, "fro") / matrix_size;
	}
	else if (dist_type == "rf-mean") {
		result = sqrt(arma::accu(arma::pow(mat_diff, 2)) / matrix_size);
	}
	else if (dist_type == "nf-norm") {
		result = arma::norm(mat_diff, "fro") / arma::norm(mat_old, "fro");
	}
	else{
		std::cout << "Unknown matrix distance type: " << dist_type << std::endl;
		exit(1);
	}

	return result;
}

/*
  name: compute_Phi_mat
  description: compute the Phi matrix for m_step
  paramemters:
  		X - the training data matrix
		B - current estimate of B matrix
		Theta - a list of MH samples
  return:
  		arma::mat - Phi matrix
 */
arma::mat compute_Phi_mat(arma::mat X, arma::mat B, mat_vec Theta){
	uint n = X.n_rows, m = Theta[0].n_rows, q = Theta[0].n_cols;
	arma::mat Phi(n * m, q, arma::fill::zeros);

	for (uint i = 0; i < n; ++i) {
		arma::mat X_i = arma::repmat(X.row(i), m, 1);
		Phi(arma::span(i * m, (i+1) * m - 1), arma::span::all) = 
			arma::log(Theta[i]) - X_i * B;
	} /* end of loop for(i) */

	return Phi;
}

/*
  name: compute_f1_score
  description: compute the f1 score the of estimated B matrix
  paramemters: 
  		B_true - true B matrix
		B_est - estimated B matrix
  return: 
  		double - f1 score
 */
double compute_f1_score(arma::mat B_true, arma::mat B_est){
	uint non_zero_B = 0, non_zero_B_est = 0, non_zero_correct = 0;
	uint p = B_true.n_rows, q = B_true.n_cols;
	double small_value = 1.0e-10;

	for (uint row = 0; row < p; ++row) {
		for (uint col = 0; col < q; ++col) {
			if (B_true(row, col) != 0)
				non_zero_B += 1;

			if (B_est(row, col) < -small_value || B_est(row,col) > small_value)
				non_zero_B_est += 1;

			if ((B_true(row, col) != 0) && (B_est(row, col) < -small_value || 
						B_est(row,col) > small_value))
				non_zero_correct += 1;
		} /* end of loop for(col) */
	} /* end of loop for(row) */

	double precision = double(non_zero_correct) / double(non_zero_B_est);
	double recall = double(non_zero_correct) / double(non_zero_B);
	double fscore = 2 * precision * recall / (precision + recall);

	return fscore;
}

/*
  name: compute_false_pos_neg_rate
  description: compute the false positive and false negative rate
  paramemters: 
  		B_true - true B matrix
		B_est - estimated B matrix
  return:
  		pair<double, double> - std::pair of false postive rate and false 
							   negative rate
 */
std::pair<double, double> compute_false_pos_neg_rate(arma::mat B_true, 
													 arma::mat B_est){
	uint non_zero_B = 0, false_positive = 0, false_negative = 0;
	uint p = B_true.n_rows, q = B_true.n_cols;
	double small_value = 1.0e-10;

	for (uint row = 0; row < p; ++row) {
		for (uint col = 0; col < q; ++col) {
			if (B_true(row, col) != 0)
				non_zero_B += 1;

			/* false positive */
			if (B_true(row, col) == 0 && (B_est(row, col) < -small_value || 
						B_est(row, col) > small_value))
				false_positive += 1;

			/* false negative */
			if (B_true(row, col) != 0 && (B_est(row, col) > -small_value && 
						B_est(row, col) < small_value))
				false_negative += 1;
		} /* end of loop for(col) */
	} /* end of loop for(row) */

	double false_pos_rate = double(false_positive) / double(p * q - non_zero_B);
	double false_neg_rate = double(false_negative) / double(non_zero_B);

	return std::make_pair(false_pos_rate, false_neg_rate);
}

/*
  name: nearPSD
  description: compute the nearest positive semidefinite matrix of the given 
  			   matrix. Implement Nicholas J. Higham's algorithm in the paper: 
			   Computing the nearest correlation matrix -- a problem from
			   finance. IMA Journal of Numerical Analysis (2002) 22, 329-343.
  paramemters: the same with the R function nearPD.
  return:
  		arma::mat - the nearest PSD matrix
 */
PSD_result nearPSD(arma::mat x, uint maxit, bool corr, bool keepDiag, 
		bool do2eigen, bool doDykstra, bool ensureSymmetry, double eig_tol, 
		double conv_tol, double posd_tol) {
	/* check whether it is a square matrix */
	assert(x.n_rows == x.n_cols);

	uint n = x.n_rows;
	/* if not symmetric matrix, use its symmetric part instead */
	if (ensureSymmetry){
		x = (x + x.t()) / 2;
	}

	/* whether to keep diagnal */
	arma::vec diag_vec;
	if (keepDiag) {
		diag_vec = x.diag();
	}
	/* whether to do Dykstra corrections */
	arma::mat D_S;
	if (doDykstra) {
		D_S.set_size(n, n);
		D_S.fill(0);
	}

	/* begin the iterations */
	arma::mat X(x), R;
	uint iter = 0;
	bool converged = false;
	double conv = std::numeric_limits<double>::max();
	while (iter < maxit && !converged) {
		arma::mat Y(X);
		if (doDykstra) {
			R.set_size(n, n);
			R = Y - D_S;
		}

		arma::vec d;
		arma::mat Q;
		if (doDykstra){
			arma::eig_sym(d, Q, arma::symmatl(R));
		}
		else {
			arma::eig_sym(d, Q, arma::symmatl(Y));
		}

		/* check non NSD */
		double non_negative = arma::any(d > arma::max(d) * eig_tol);
		if(!non_negative){		/* NSD */
			std::cerr << "Matrix seems negative semi-definite." << std::endl
				<< (doDykstra ? R : Y) << std::endl;
			std::cerr << "eigen values: " << std::endl << d.t() << std::endl;
			exit(1);
		}

		/* compute the A_+ */
		arma::uvec indecies = arma::find(d < arma::max(d) * eig_tol);
		d.rows(indecies).fill(0);
		X = Q * arma::diagmat(d) * Q.t();

		if (doDykstra) {
			D_S = X - R;
		}

		/* check if it is correlation matrix */
		if (corr) {
			X.diag().fill(1);
		}
		else if (keepDiag){
			X.diag() = diag_vec;
		}

		/* compute the convergence criterion */
		arma::mat diff = Y - X;
		conv = matrix_norm(diff, 'I') / matrix_norm(Y, 'I');
		iter += 1;
		converged = (conv <= conv_tol);
	} /* end of loop while */

	/* apply posdefify step to the results of Higham algorithm */
	if (do2eigen) {	
		arma::vec d;
		arma::mat Q;
		arma::eig_sym(d, Q, arma::symmatl(X));
		double Eps = posd_tol * fabs(arma::max(d));
		if (arma::min(d) < Eps) {
			d.rows(arma::find(d < Eps)).fill(Eps);
			arma::vec X_diag = X.diag();
			X = Q * arma::diagmat(d) * Q.t();
			arma::vec Eps_vec(n);
			Eps_vec.fill(Eps);
			arma::vec D = arma::sqrt(arma::max(Eps_vec, X_diag) / X.diag());
			arma::mat D_mat = arma::repmat(arma::reshape(D, D.n_elem, 1), 1, n);
			X = D_mat % X % D_mat.t();
		}

		if (corr) {
			X.diag().fill(1);
		}
		else if (keepDiag){
			X.diag() = diag_vec;
		}
	}

	PSD_result result;
	if (converged)
		result = std::make_pair(X, 0);
	else
		result = std::make_pair(X, 1);

	return result;
}

/*
  name: B_entry_accuracy
  description: compute the accumulative B elementwise estimation accuracy across 
  			   different replication of runs.
  paramemters: 
  		fileName - file name to store the B accuracy result
  return: void
 */
void B_entry_accuracy(std::string fileName, arma::mat B_est) {
	arma::mat B_accu(B_est.n_rows, B_est.n_cols);

	/* check whether the outfile exists */
	std::ifstream B_file(fileName, std::ios::in);
	if (B_file.good()) {	//file exists
		B_accu.load(B_file, arma::csv_ascii);
	}
	else{					//file does not exist, first replication
		B_accu.fill(0.0);
	}
	B_file.close();

	/* compute the non-zero entries in B_est */
	double small_value = 1.0e-10;
	arma::uvec indecies = arma::find(B_est > small_value || 
									 B_est < -small_value);
	B_accu.elem(indecies) = B_accu.elem(indecies) + 1;

	/* write the results back to file */
	std::ofstream B_output(fileName, std::ios::out);
	B_accu.save(B_output, arma::csv_ascii);
	B_output.close();
}

/*
  name: Omega_entry_accuracy
  description: compute the Omega estimation accuracy across replications
  paramemters: 
  		fileName - file name to store the Omega accuracy result
		Omega_est - estimation of Omega of current run
  return: void
 */
void Omega_entry_accuracy(std::string fileName, arma::mat Omega_est){
	arma::mat Omega_accu(Omega_est.n_rows, Omega_est.n_cols);

	/* check whether the output file exists */
	std::ifstream Omega_file(fileName, std::ios::in);
	if (Omega_file.good()) {		//file exists
		Omega_accu.load(Omega_file, arma::csv_ascii);
	}
	else {
		Omega_accu.fill(0.0);
	}
	Omega_file.close();

	/* compute the non-zero entries in Omega_est */
	double small_value = 1.0e-10;
	arma::uvec indecies = arma::find(Omega_est > small_value ||
									 Omega_est < -small_value);
	Omega_accu.elem(indecies) = Omega_accu.elem(indecies) + 1;

	/* write the result back to file */
	std::ofstream Omega_output(fileName, std::ios::out);
	Omega_accu.save(Omega_output, arma::csv_ascii);
	Omega_output.close();
}

/*
  name: converge_status_addone
  description: add 1 to the number of converged model replications
  paramemters: 
  		fileName - the file path for the convergence file
  return: void
 */
void converge_status_addone(std::string fileName){
	arma::mat converge_num(1, 1);

	/* check whether the output file exists */
	std::ifstream converge_file(fileName, std::ios::in);
	if (converge_file.good()) {
		converge_num.load(converge_file, arma::csv_ascii);
	}
	else {
		converge_num.fill(0.0);
	}
	converge_file.close();

	converge_num(0, 0) += 1;

	/* write the result back to file */
	std::ofstream converge_output(fileName, std::ios::out);
	converge_num.save(converge_output, arma::csv_ascii);
	converge_output.close();
}

/*
  name: random_Omega
  description: generate well conditioned random Omega matrix with given number 
  			   of rows and columns
  paramemters: 
  		nrow, ncol - number of rows and columns
  return: 
  		arma::mat
 */
arma::mat random_Omega(uint nrow, uint ncol){
	arma::mat omega;
	
	while (true) {
		/* generate random PSD Omega from uniform(0,1) */
		omega = arma::randn<arma::mat>(nrow, ncol);
		omega = omega.t() * omega;

		/* check the condition number of the generated matrix */
		arma::vec eigen_vals = arma::eig_sym(omega);
		double max_eigen = arma::max(eigen_vals), 
			   min_eigen = arma::min(eigen_vals);
		if (min_eigen > 0.01 && max_eigen < 100) {
			break;
		}
	} /* end of loop while */

	return omega;
}
