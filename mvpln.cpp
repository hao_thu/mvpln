/*
  file: mvpln.cpp
  description: main file of MVPLN model, estimate the model parameters, and 
  			   warp into a python module.
  author: HW
 */

#include <omp.h>
#include <iostream>
#include <algorithm>
#include "mvpln.h"
#include "logger.h"

#define _OPENMP_

/*
  name: compute_eBIC
  description: compute the extended Bayesian Information criterion of the given 
  			   model (lambda1 & lambda2 combination)
  paramemters:
  		X, Y - training data matrix
		B, Omega - model parameters
		lambda1, lambda2 - regularization parameters
		gamma - parameter for eBIC measure
		tau, prop_type - paramters for mcmc procedure
		fact_table - factorial table in log scale
  return:
  		double - eBIC score
 */
double compute_eBIC(arma::mat X, arma::mat Y, arma::mat B, arma::mat Omega,
					double lambda1, double lambda2, double gamma, double tau, 
					uint N, double_vec &fact_table, std::string prop_type){
	uint n = X.n_rows, p = B.n_rows, q = B.n_cols;

	/* compute the number of non-zero entries in B and Omega */
	B.elem(arma::find(B > -1.0e-10 && B < 1.0e-10)).fill(0);
	Omega.elem(arma::find(Omega > -1.0e-10 && Omega < 1.0e-10)).fill(0);
	arma::sp_mat sp_B(B), sp_Omega(Omega);
	uint v_B = sp_B.n_nonzero, v_Omega = sp_Omega.n_nonzero;

	/* run the MH sampling to compute the approximate log-likelihood */
	mat_vec Thetas = mc_e_step(X, Y, B, Omega, N, fact_table, prop_type, tau);
	uint m = Thetas[0].n_rows;
	arma::mat Phi = compute_Phi_mat(X, B, Thetas);
	double neg_log_likelihood = arma::trace(Phi.t() * Phi * Omega) / 
		double(m * n) - log(arma::det(Omega));

	/* compute eBIC */
	double eBIC = 2 * neg_log_likelihood + (v_B + v_Omega) * log(n) + 
		2 * gamma * v_B * log(p * q) + 4 * gamma * v_Omega * log(q);

	return eBIC;
}

/*
  name: estimate_parameters
  description: estimate the B and Omega matrix for the given lambda1 and lambda2
  paramemters:	
  		X, Y - training data matrix
		B, Omega - initial model parameters
		lambda1, lambda2 - regularization parameters
		epsilon - stopping criterion of iterations
		tau, N, prop_type - paramters for mcmc procedure
		fact_table - factorial table in log scale
		maxiter - maximum allowed iterations for EM algorithm.
  return:
  		mat_pair - pair of estimate B and Omega matrix.
 */
model_param estimate_parameters(arma::mat X, arma::mat Y, arma::mat B, 
							 arma::mat Omega, double lambda1, double lambda2,
							 double epsilon, double gamma, double tau, uint N, 
							 double_vec &fact_table, std::string dist_type,
							 std::string prop_type, uint maxiter){
	/* some initialization */
	arma::mat B_prev = B, Omega_prev = Omega;
	arma::mat B_curr, Omega_curr;
	uint n = X.n_rows, iters = 0;

	/* begin the MCEM algorithm to estimate the model paramemters */
	while (true) {
		iters += 1;

		/* MC E-step */
		mat_vec Thetas = mc_e_step(X, Y, B_prev, Omega_prev, N, fact_table, 
				prop_type, tau);

		/* M-step */
		mat_pair B_Omega_curr = m_step(X, B_prev, Omega_prev, Thetas, lambda1, 
				lambda2, epsilon, dist_type, 500);
		B_curr = B_Omega_curr.first, Omega_curr = B_Omega_curr.second;

		/* compute the stop criterion */
		double B_stop = matrix_distance(B_curr, B_prev, dist_type);
		double Omega_stop = matrix_distance(Omega_curr, Omega_prev, dist_type);
		if (iters == maxiter || (B_stop < epsilon && Omega_stop < epsilon)) {
			break;
		}

		/* prepare for next iteration */
		B_prev = B_curr;
		Omega_prev = Omega_curr;
	} /* end of loop while */

	model_param result;
	result.B = B_curr, result.Omega = Omega_curr;
	result.lambda1 = lambda1, result.lambda2 = lambda2, result.niters = iters;
	if (iters == maxiter) {
		result.status = "Maximum allowed iteration reached.";
		result.converged = false;
	}
	else {
		result.status = "converged.";
		result.converged = true;
	}
	/* compute eBIC score of the model */
	double eBIC_score = compute_eBIC(X, Y, B_curr, Omega_curr, lambda1, lambda2,
			gamma, tau, N, fact_table, prop_type);
	result.eBIC = eBIC_score;

	return result;
}

/*
  name: predict_mvpln
  description: make prediction with the given model parameters
  paramemters: 
  		X - matrix of predictors used for prediction
		B - coefficient matrix of the MVPLN model
  return:
  		arma::mat - precited reponse matrix
 */
arma::mat predict_mvpln(arma::mat X, arma::mat B) {
	/* compute the thetas */
	arma::mat Thetas = arma::exp(X * B);
	uint n = X.n_rows, q = B.n_cols;
	arma::mat Y(n, q, arma::fill::zeros);

	for (uint i = 0; i < n; ++i) {
		arma::vec mu_i = arma::vectorise(Thetas.row(i));
		Y.row(i) = arma::mat(multivariate_poisson(mu_i, q).t());
	} /* end of loop for(i) */

	return Thetas;
}

/*
  name: model_selection
  description: selelct best model from the given range of lambda1 and lambda2 
  			   with eBIC measure.
  paramemters:	
  		X, Y - training data matrix
		B, Omega - initial model parameters
		lambda1_vec, lambda2_vec - ranges of regularization parameters
		epsilon - stopping criterion of iterations
		tau, N, prop_type - paramters for mcmc procedure
		fact_table - factorial table in log scale
		maxiter - maximum allowed iterations for EM algorithm.
  return:
  		selected lambda1, lambda2, B and Omega matrix.
 */
model_param model_selection(arma::mat X, arma::mat Y, arma::mat B, 
							arma::mat Omega, double_vec lambda1_vec, 
							double_vec lambda2_vec, double epsilon, double tau, 
							double gamma, uint N, double_vec &fact_table, 
							std::string dist_type, std::string prop_type, 
							uint maxiter){
	std::vector<model_param> model_vec;

#ifdef _OPENMP_
#pragma omp parallel for schedule(dynamic, 1) shared(model_vec) collapse(2)
#endif
	for (uint lam1 = 0; lam1 < lambda1_vec.size(); ++lam1) {
		for (uint lam2 = 0; lam2 < lambda2_vec.size(); ++lam2) {
			/* estimate model parameters */
			model_param result = estimate_parameters(X, Y, B, Omega, 
					lambda1_vec[lam1], lambda2_vec[lam2], epsilon, gamma, tau, 
					N, fact_table, dist_type, prop_type, maxiter);

			/* compute eBIC score */
#ifdef _OPENMP_
#pragma omp critical
#endif
			{
				model_vec.push_back(result);
			}
		} /* end of loop for(lam2) */
	} /* end of loop for(lam1) */

	/* sort the model based on eBIC score */
	struct model_param_less {
		bool operator() (const model_param &lhs, const model_param &rhs){
			return (lhs.eBIC < rhs.eBIC);
		}
	} comparator;
	std::sort(model_vec.begin(), model_vec.end(), comparator);

	return model_vec[0];
}
