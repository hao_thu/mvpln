/*
  file: utils.h
  description: define the util functions that will be used in MVPLN model
  author: HW
 */

#ifndef UTILS_H
#define UTILS_H

#define ARMA_DONT_USE_CXX11

#include "armadillo"
#include <boost/python.hpp>
#include <string.h>
#include <vector>
#include <utility>
#include <math.h>

namespace bp = boost::python;

typedef unsigned int uint;
typedef std::vector<double> double_vec;
typedef std::pair<arma::mat, uint> PSD_result;
typedef std::vector<arma::mat> mat_vec;

arma::mat convert2mat(bp::list py_mat);

bp::list convert2pylist(arma::mat arma_mat);

arma::vec multivariate_poisson(arma::vec mu, uint dim);

double matrix_norm(arma::mat x, char norm_type = 'F');

double matrix_distance(arma::mat mat_new, arma::mat mat_old, 
					   std::string dist_type = "f-mean");

arma::mat compute_Phi_mat(arma::mat X, arma::mat B, mat_vec Theta);

double compute_f1_score(arma::mat B_true, arma::mat B_est);

std::pair<double, double> compute_false_pos_neg_rate(arma::mat B_true, 
													 arma::mat B_est);

PSD_result nearPSD(arma::mat x, uint maxit = 100, bool corr = false, 
				  bool keepDiag = false, bool do2eigen = true, 
				  bool doDykstra = true, bool ensureSymmetry = true, 
				  double eig_tol = 1.0e-6, double conv_tol = 1.0e-7, 
				  double posd_tol = 1.0e-8);

void B_entry_accuracy(std::string fileName, arma::mat B_est);

void Omega_entry_accuracy(std::string fileName, arma::mat Omega_est);

void converge_status_addone(std::string fileName);

arma::mat random_Omega(uint nrow, uint ncol);

/*
  name: prepare_log_factorial
  description: prepare the factorial table in log scale
  paramemters:
  		y_max - the maximum value in the response matrix Y.
  return:
  		double_vec - the factorial table in log scale
 */
inline double_vec prepare_log_factorial(uint y_max) {
	double_vec result(y_max + 1, 0);

	for (uint i = 1; i < y_max + 1; ++i) {
		result[i] = result[i - 1] + log(i);
	} /* end of loop for(i) */

	return result;
}

/*
  name: log_factorial
  description: compute the factorial for each dimension of vec_x in log scale
  paramemters: 
  		vec_x - the give reference to arma::vec
		fact_table - log factorial table to compute factorial.
  return: return type
 */
inline arma::vec log_factorial(arma::vec vec_x, double_vec &fact_table){
	arma::vec result(vec_x.n_elem);
	result.fill(0.0);
	for (uint i = 0; i < vec_x.n_elem; ++i) {
		result(i) = fact_table[vec_x(i)];
	} /* end of loop for(i) */

	return result;
}

#endif
