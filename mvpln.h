/*
  file: mvpln.h
  description: declare the interface function for mvpln model
  author: HW
 */

#ifndef MVPLN_H
#define MVPLN_H

#include "mcem.h"

typedef struct {
	arma::mat B;
	arma::mat Omega;
	uint niters;
	double lambda1, lambda2;
	std::string status;
	double eBIC;
	bool converged;
} model_param;

double compute_eBIC(arma::mat X, arma::mat Y, arma::mat B, arma::mat Omega,
					double lambda1, double lambda2, double gamma, double tau, 
					uint N, double_vec &fact_table,std::string prop_type = "N");

model_param estimate_parameters(arma::mat X, arma::mat Y, arma::mat B, 
							 arma::mat Omega, double lambda1, double lambda2,
							 double epsilon, double gamma, double tau, uint N, 
							 double_vec &fact_table, 
							 std::string dist_type = "f-mean",
							 std::string prop_type = "N", uint maxiter = 100);

arma::mat predict_mvpln(arma::mat X, arma::mat B);

model_param model_selection(arma::mat X, arma::mat Y, arma::mat B, 
							arma::mat Omega, double_vec lambda1_vec, 
							double_vec lambda2_vec, double epsilon, double tau, 
							double gamma, uint N, double_vec &fact_table, 
							std::string dist_type = "f-mean",
							std::string prop_type = "N", uint maxiter = 100);
#endif
