/*
  file: logger.cpp
  description: implementation of the Logger class
  author: HW
 */

#include <algorithm>
#include "logger.h"

/* initialize the static member */
Logger* Logger::logger_ = PTR_NULL;

/* implement member function */

/*
  name: getInstance
  description: return the Logger instance, or create an logger instance if it 
  			   does not exist.
  paramemters:
  		logFile: file name of the log.
  return: 
  		pointer to the Logger object.
 */
Logger* Logger::getInstance(std::string logFile){
	if (Logger::logger_ == PTR_NULL) {
		logging::register_simple_formatter_factory<
			logging::trivial::severity_level, char>("Severity");
		logging::add_file_log(
			keywords::file_name = logFile,
			keywords::format = "%TimeStamp% <%Severity%>: %Message%",
			keywords::open_mode = (std::ios::out | std::ios::app),
			keywords::auto_flush = true);

		logging::core::get()->set_filter(
			logging::trivial::severity >= logging::trivial::debug
		);

		logging::add_common_attributes();
		Logger::logger_ = new Logger();
	}/* end of if */

	return Logger::logger_;
}

/*
  name: logInfo
  description: write log message at info severity.
  paramemters: 
  		message: log message.
  return: 
  		void
 */
void Logger::logInfo(std::string message){
	BOOST_LOG_SEV(log_, logging::trivial::info) << message;
}

/*
  name: logDebug
  description: write the log message at debug level
  paramemters: 
		message: log message
  return: 
  		void
 */
void Logger::logDebug(std::string message){
	BOOST_LOG_SEV(log_, logging::trivial::debug) << message;
}

/*
  name: logWarn
  description: write the warning log message
  paramemters: 
  		message: log message
  return: 
  		void
 */
void Logger::logWarn(std::string message){
	BOOST_LOG_SEV(log_, logging::trivial::warning) << message;
}

/*
  name: logError
  description: write the error log message
  paramemters: 
  		message: log message
  return: 
  		void
 */
void Logger::logError(std::string message){
	BOOST_LOG_SEV(log_, logging::trivial::error) << message;
}

/*
  name: logFatal
  description: write the fatal log message
  paramemters: 
  		message: log message
  return: 
  		void
 */
void Logger::logFatal(std::string message){
	BOOST_LOG_SEV(log_, logging::trivial::fatal) << message;
}

void Logger::write_log(std::string message, std::string level){
	std::transform(level.begin(), level.end(), level.begin(), ::tolower);

	if (level == "warning")
		logWarn(message);
	else if (level == "debug")
		logDebug(message);
	else if (level == "error")
		logError(message);
	else if (level == "fatal")
		logFatal(message);
	else
		logInfo(message);
}
