/*
  file: mcmc_mh.h
  description: declare the Metropolis sampling related functions
  author: HW
 */

#ifndef MCMC_MH_H
#define MCMC_MH_H

#include "utils.h"

typedef std::pair<arma::vec, arma::mat> vec_mat_pair;

arma::vec multivariate_normal_distribution (arma::vec mu, arma::mat cov);

arma::vec r_proposal_distribution(arma::vec vec_loc, arma::mat cov,
								  std::string prop_type = "N");

double proposal_density_log(arma::vec vec_x, arma::vec vec_loc, 
							std::string prop_type, arma::mat cov);

double target_density_log(arma::vec vec_theta, arma::vec vec_x, 
						  arma::vec vec_y, arma::mat B, arma::mat Sigma, 
						  double_vec &fact_table);

arma::mat compute_hessian(arma::vec vec_theta, arma::vec vec_x, 
						  arma::vec vec_y, arma::mat B, arma::mat Sigma);

vec_mat_pair find_mode_cov_approx (arma::vec init_theta, arma::vec vec_x, 
								   arma::vec vec_y, arma::mat B, 
								   arma::mat Sigma, double tau);

arma::mat MH_sampling(arma::vec init_theta, arma::vec vec_x, arma::vec vec_y,
					  arma::mat B, arma::mat Sigma, uint N, 
					  double_vec &fact_table, std::string prop_type, 
					  arma::mat cov);

#endif
