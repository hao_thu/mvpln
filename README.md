# Multivariate Possion Log-Normal Model for Count Data #

C++ implementation of the multivariate Possion log-normal model for the paper:

**H. Wu**, X. Deng, and N. Ramakrishnan, ''Sparse estimation of multivariate
Poisson log-Normal models from count data'', Statistical Analysis and Data
Mining: The ASA Data Science Journal, preprint version available
at [arXiv:1602.07337v2 [stat.ME]](https://arxiv.org/abs/1602.07337).

This C++ code impement a Monte Carlo Expectation-Maximization algorithm for
sparse estimation of the multivariate Poisson log-normal regression model. In
the E-step, Metroplis sampling is used to compute the approximate expected
log-likelihood, and in the M-step, the approximated expected log-likelihood is
maximized via an alternating gradient based algorithm. For details, please refer
to the corresponding paper mentioned above.

## Compilation ##

To compile the code, simple execute make in the root directory of the code, e.g.

	cd ./mvpln
	make

The compiled executable will be in the current directory.

**Required library**: C++ Boost, Boost log Boost Python, Armadillo, gfortran
compiler, and OpenMP for parallel computation.

**Notice**: Although the implmented model is wrapped into a shared library (.so
file) with Boost python so that it can be used in python script, it is not fully
tested with python yet.
