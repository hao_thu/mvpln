/*
  file: mvpln_realData.cpp
  description: apply mvpln model over real event dataset
  author: HW
 */

#include "mvpln.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include <stdlib.h>

#define _PARALLEL_
//#define _SINGLE_

int main(int argc, const char *argv[])
{
	if (argc != 10) {
		std::cout << "The number of arguments is not correct!" << std::endl;
		exit(1);
	}

	/* load the data from csv file */
	std::string X_file(argv[6]);
	std::string Y_file(argv[7]);
	std::string X_test_file(argv[8]);
	std::string Y_test_file(argv[9]);

	arma::mat X, Y, X_test, Y_test;
	X.load(X_file, arma::csv_ascii);
	Y.load(Y_file, arma::csv_ascii);
	X_test.load(X_test_file, arma::csv_ascii);
	Y_test.load(Y_test_file, arma::csv_ascii);

	/* some initialization */
	uint y_max = arma::max(arma::vectorise(Y));
	double_vec fact_table = prepare_log_factorial(y_max);
	uint p = X.n_cols, q = Y.n_cols;
	arma::mat B_init = arma::ones<arma::mat>(p, q) / 10.0;
	arma::mat Omega_init = random_Omega(q, q);

#ifdef _PARALLEL_
	{
		/* read command line arguments */
		double epsilon = atof(argv[1]), gamma = atof(argv[2]), 
			   tau = atof(argv[3]);
		uint N = atoi(argv[4]);
		uint maxiter = atoi(argv[5]);

		double_vec lambda1_vec, lambda2_vec;
		double step = 0.2, lam_start = -2.8, lam_end = 0;
		for (uint i = 0; i < 10; ++i) {
			double lam_value = pow(10, lam_start + step * i);
			lambda1_vec.push_back(lam_value);
			lambda2_vec.push_back(lam_value);
		}

		/* perform the model selelction */
		model_param best_model = model_selection(X, Y, B_init, Omega_init, 
				lambda1_vec, lambda2_vec, epsilon, tau, gamma, N, fact_table, 
				"rf-mean", "N", maxiter);
		std::cout << "The best model: " << std::endl << "B matrix: " 
			<< std::endl << best_model.B << std::endl << "Omega matrix: " 
			<< std::endl << best_model.Omega << std::endl << "status: "
			<< best_model.status << std::endl << "number of EM iterations: "
			<< best_model.niters << std::endl << "eBIC score: " 
			<< best_model.eBIC << std::endl << "lambda1: " << best_model.lambda1 
			<< ", lambda2: " << best_model.lambda2 << std::endl;

		arma::mat Y_est = predict_mvpln(X_test, best_model.B);
		arma::rowvec mse = arma::sum(arma::pow(Y_test - Y_est, 2), 0) / 
			double(Y_test.n_rows);
		arma::rowvec rmse = arma::sqrt(mse);
		std::cout << "rMSE: " << rmse << std::endl;

		/* output the Y estimation */
		Y_est.save("./Y_est_mvpln.csv", arma::csv_ascii);
        best_model.Omega.save("./Omega_est_mvpln.csv", arma::csv_ascii);
		
	}
#endif

#ifdef _SINGLE_
	{	
		/* read the input parameters */
		if (argc != 7){
			std::cout << "Not enough parameters!" << std::endl;
			exit(1);
		}

		double lambda1 = atof(argv[1]), lambda2 = atof(argv[2]), 
			   epsilon = atof(argv[3]), gamma = atof(argv[4]), tau = atof(argv[5]);
		uint N = atoi(argv[6]);

		/* estimate the model */
		model_param result_model = estimate_parameters(X, Y, B_init, Omega_init, 
				lambda1, lambda2, epsilon, gamma, tau, N, fact_table, "rf-mean");

		/* write the model parameters (eBIC, lambda1, lambda2) into file */
		std::ofstream param_file;
		param_file.open("./intermediate_result/eBIC_lambda1_lambda2.csv", 
				std::ios::out | std::ios::app);
		param_file << result_model.eBIC << "," << result_model.lambda1 << ","
			<< result_model.lambda2 << std::endl;
		param_file.close();
	}
#endif
	return 0;
}
