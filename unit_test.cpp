/*
  file: unit_test.cpp
  description: perform unit test for each function of the MVPLN model
  author: HW
 */

//#define _OPENMP_
//#define _TEST_UTILS_
//#define _TEST_MCMC_MH_
//#define _TEST_MCEM_
//#define _TEST_MVPLN_
#define _MAIN_

#include "mvpln.h"
#include <iostream>
#include <fstream>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main(int argc, const char *argv[])
{
	/* load the data from csv file */
	std::string B_file = "../r_script/B.csv";
	std::string Sigma_file = "../r_script/Sigma.csv";
	std::string X_file = "../r_script/X.csv";
	std::string Y_file = "../r_script/Y.csv";
	std::string X_test_file = "../r_script/X_test.csv";
	std::string Y_test_file = "../r_script/Y_test.csv";

	arma::mat B_true, Sigma_true, X, Y, X_test, Y_test;
	B_true.load(B_file, arma::csv_ascii);
	Sigma_true.load(Sigma_file, arma::csv_ascii);
	X.load(X_file, arma::csv_ascii);
	Y.load(Y_file, arma::csv_ascii);
	X_test.load(X_test_file, arma::csv_ascii);
	Y_test.load(Y_test_file, arma::csv_ascii);

	/* test method declared in utils.h */
#ifdef _TEST_UTILS_
	{
		std::cout << "test functions in utils.h and utils.cpp" << std::endl;
		//matrix_norm
		//double mat_norm_f = matrix_norm(Sigma_true, 'F');
		//double mat_norm_i = matrix_norm(Sigma_true, 'I');
		//std::cout << "F-Norm: " << mat_norm_f << " , Infinity Norm: " << mat_norm_i
			//<< std::endl;

		//matrix_distance
		//arma::arma_rng::set_seed_random();
		//arma::mat Sigma_random = arma::randn<arma::mat>(Sigma_true.n_rows, 
				//Sigma_true.n_cols);
		//std::cout << "random generated matrix: " << std::endl << Sigma_random 
			//<< std::endl;
		//double mat_dist_fmean = matrix_distance(Sigma_true, Sigma_random, "f-mean");
		//double mat_dist_rfmean = matrix_distance(Sigma_true, Sigma_random, "rf-mean");
		//double mat_dist_nfmean = matrix_distance(Sigma_true, Sigma_random, "nf-norm");
		//double mat_dist_nfnorm = matrix_distance(Sigma_random, Sigma_true, "nf-norm");
		//std::cout << "f-mean distance: " << mat_dist_fmean << " , " 
			//<< "rf_mean distance: " << mat_dist_rfmean << " , "
			//<< "nf_mean distance: " << mat_dist_nfmean << ", " 
			//<< "nf_norm distance reverse: " << mat_dist_nfnorm << std::endl;

		//compute_Phi_mat: leave to the section when test MH sampling
		
		//multivariate_poisson
		//arma::vec mu = arma::vectorise(Y.row(0));
		//std::cout << "mean of the multivariate poisson distribution: " << std::endl
			//<< mu << std::endl;
		//for (uint i = 0; i < 20; ++i) {	
			//arma::vec rv_poisson = multivariate_poisson(mu, mu.n_elem);
			//std::cout << "multivariate poisson sample: " << std::endl << rv_poisson.t() 
				//<< std::endl;
		//} [> end of loop for(i) <]

		//compute_f1_score
		//double f1_score = compute_f1_score(B_true, B_true);
		//std::cout << "f1_score: " << f1_score << std::endl;

		//nearPSD
		arma::mat test_mat(2, 2, arma::fill::zeros);
		test_mat(0,0) = -0.0322, test_mat(1,1) = 1.6726;
		//PSD_result psd_pair = nearPSD(Sigma_random);
		PSD_result psd_pair = nearPSD(test_mat);
		std::cout << "converge status: " << psd_pair.second << std::endl;
		psd_pair.first.print();
		arma::vec eig_vals = arma::conv_to<arma::vec>::from(arma::eig_gen(psd_pair.first));
		eig_vals.t().print();
	}
#endif

	/* test methods in files: mcmc_mh.h & mcmc_mh.cpp */
#ifdef _TEST_MCMC_MH_
	{
		arma::arma_rng::set_seed_random();
		//multivariate_normal_distribution
		arma::vec mu = arma::randn<arma::vec>(5);
		arma::mat cov = arma::randn<arma::mat>(5, 5);
		cov = cov.t() * cov;
		std::cout << "multivariate normal distribution:" << std::endl << "mu: "
			<< mu.t() << std::endl << "covariance matrix: " << std::endl 
			<< cov << std::endl;
		arma::mat samples(20000, 5, arma::fill::zeros);
		for (uint i = 0; i < 20000; ++i) {
			samples.row(i) = multivariate_normal_distribution(mu, cov).t();
		} /* end of loop for(i) */
		std::cout << "empirical covariacne matrix: " << std::endl 
			<< arma::cov(samples) << std::endl;

		//r_proposal_distribution
		arma::vec new_mu = mu + 5;
		arma::mat new_cov = cov / 10;
		arma::vec theta_cand = r_proposal_distribution(new_mu, new_cov, "N");
		theta_cand.t().print();

		//target_density_log
		arma::vec vec_x = arma::vectorise(X.row(0));
		arma::vec vec_y = arma::vectorise(Y.row(0));
		uint y_max = arma::max(arma::vectorise(Y));
		double_vec fact_table = prepare_log_factorial(y_max);
		double target_value = target_density_log(vec_y, vec_x, vec_y, B_true, 
				Sigma_true, fact_table);
		std::cout << "target density value in log scale: " << target_value 
			<< std::endl;

		//compute_hessian
		arma::mat hessian = compute_hessian(vec_y, vec_x, vec_y, B_true, 
				Sigma_true);
		std::cout << "Hessian matrix: " << std::endl << hessian << std::endl;

		//find_mode_cov_approx
		double tau = 0.1;
		vec_mat_pair mode_cov = find_mode_cov_approx(vec_y, vec_x, vec_y, 
				B_true, Sigma_true, tau);
		std::cout << "mode of the target density: " << std::endl 
			<< mode_cov.first.t() << std::endl 
			<< "estimated covariance matrix:" << std::endl << mode_cov.second
			<< std::endl;

		//MH_sampling
		arma::mat thetas = MH_sampling(mode_cov.first, vec_x, vec_y, B_true, 
				Sigma_true, 5000, fact_table, "N", mode_cov.second);
		thetas.save("./Thetas.csv", arma::csv_ascii);
	}
#endif

	/* test methods in files: mcem.h & mcem.cpp */
#ifdef _TEST_MCEM_
	{
		uint y_max = arma::max(arma::vectorise(Y));
		double_vec fact_table = prepare_log_factorial(y_max);
		double tau = 0.1;

		//mc_e_step
		arma::mat Omega_true = Sigma_true.i();
		mat_vec Thetas = mc_e_step(X, Y, B_true, Omega_true, 300, fact_table, 
				"N", tau);
		for (uint i = 0; i < Thetas.size(); ++i) {
			Thetas[i].save("./thetas/" + std::to_string(i) + ".csv", arma::csv_ascii);
		} /* end of loop for(i) */

		//compute_Phi_mat
		arma::mat Phi = compute_Phi_mat(X, B_true, Thetas);
		Phi.save("./Phi.csv", arma::csv_ascii);
		//std::cout << "Phi matrix: " << std::endl << Phi << std::endl;

		//estimate_Omega_matrix
		uint m = Thetas[0].n_rows, n = X.n_rows;
		arma::mat S = Phi.t() * Phi / double(m * n);
		arma::mat Omega_est = estimate_Omega_matrix(S, 0.01);
		std::cout << "estimated Omega matrix: " << std::endl << Omega_est 
			<< std::endl;

		//estimate_B_matrix_approx
		arma::mat B_est = estimate_B_matrix_approx(X, B_true, Omega_true, 
				Thetas, 0.01);
		std::cout << "estimated B matrix: " << std::endl << B_est << std::endl;

		//m_step
		mat_pair B_Omega_est = m_step(X, B_true, Omega_true, Thetas, 0.01, 0.01,
				0.01);
		std::cout << "estimated B matrix: " << std::endl << B_Omega_est.first
			<< std::endl << "true B matrix: " << std::endl << B_true 
			<< std::endl;
			
		std::cout << "estimated Omega matrix: " << std::endl
			<< B_Omega_est.second << std::endl << "true Omega matrix: " 
			<< std::endl << Omega_true << std::endl;
	}
#endif

#ifdef _TEST_MVPLN_
	{
		uint y_max = arma::max(arma::vectorise(Y));
		double_vec fact_table = prepare_log_factorial(y_max);
		double tau = 0.1;
		arma::mat Omega_true = Sigma_true.i();
		//mat_vec Thetas = mc_e_step(X, Y, B_true, Omega_true, 300, fact_table, 
				//"N", tau);
		//mat_pair B_Omega_est = m_step(X, B_true, Omega_true, Thetas, 0.01, 0.01,
				//0.01);
		//arma::mat B_est = B_Omega_est.first, Omega_est = B_Omega_est.second;
			
		//compute_eBIC
		//double eBIC_score = compute_eBIC(X, Y, B_est, Omega_est, 0.01, 0.01, 
				//0.5, 0.1, 300, fact_table);
		//std::cout << "B estimate: " << std::endl << B_est << std::endl 
			//<< "Omega estimate: " << std::endl << Omega_est << std::endl;
		//std::cout << "eBIC score: " << eBIC_score << std::endl;

		//estimate_parameters
		arma::mat B_init = arma::ones<arma::mat>(B_true.n_rows, B_true.n_cols) / 10.0;
		arma::mat Omega_init = arma::randn<arma::mat>(Omega_true.n_rows, 
				Omega_true.n_cols);
		Omega_init = Omega_init.t() * Omega_init;
		//model_param result_model = estimate_parameters(X, Y, B_init, Omega_init,
				//0.01, 0.001, 0.01, 0.5, 0.1, 300, fact_table, "rf-mean");
		//std::cout << "estimation result: " << std::endl << "B matrix: " 
			//<< std::endl << result_model.B << std::endl << "Omega matrix: " 
			//<< std::endl << result_model.Omega << std::endl << "status: "
			//<< result_model.status << std::endl << "number of EM iterations: "
			//<< result_model.niters << std::endl << "eBIC score: " 
			//<< result_model.eBIC << std::endl;

		//predict_mvpln
		//arma::mat Y_est = predict_mvpln(X_test, result_model.B);
		//std::cout << "Prediction on test data: " << std::endl << Y_est 
			//<< std::endl;
		//arma::rowvec mse = arma::sum(arma::pow(Y_test - Y_est, 2), 0) / 
			//double(Y_test.n_rows);
		//arma::rowvec rmse = arma::sqrt(mse);
		//std::cout << "rMSE: " << std::endl << rmse << std::endl;

		//model_selection
		double_vec lambda1_vec, lambda2_vec;
		//lambda1_vec.push_back(0.01), lambda1_vec.push_back(0.02);
		//lambda2_vec.push_back(0.01), lambda2_vec.push_back(0.02);

		double step = 0.2, lam_start = -2.8, lam_end = 0;
		for (uint i = 0; i < 10; ++i) {
			double lam_value = pow(10, lam_start + step * i);
			lambda1_vec.push_back(lam_value);
			lambda2_vec.push_back(lam_value);
		}
		std::cout << "number of lambda1: " << lambda1_vec.size() 
			<< ", number of lambda2: " << lambda2_vec.size() << std::endl;

		model_param best_model = model_selection(X, Y, B_init, Omega_init, 
				lambda1_vec, lambda2_vec, 0.01, 0.1, 0.5, 300, fact_table, 
				"rf-mean");
		std::cout << "The best model: " << std::endl << "B matrix: " 
			<< std::endl << best_model.B << std::endl << "Omega matrix: " 
			<< std::endl << best_model.Omega << std::endl << "status: "
			<< best_model.status << std::endl << "number of EM iterations: "
			<< best_model.niters << std::endl << "eBIC score: " 
			<< best_model.eBIC << std::endl;

		arma::mat Y_est = predict_mvpln(X_test, best_model.B);
		std::cout << "Prediction on test data: " << std::endl << Y_est 
			<< std::endl;
		arma::rowvec mse = arma::sum(arma::pow(Y_test - Y_est, 2), 0) / 
			double(Y_test.n_rows);
		arma::rowvec rmse = arma::sqrt(mse);
		std::cout << "rMSE: " << std::endl << rmse << std::endl;
	}
#endif

#ifdef _MAIN_
	{
		/* some initialization */
		uint y_max = arma::max(arma::vectorise(Y));
		double_vec fact_table = prepare_log_factorial(y_max);
		arma::mat Omega_true = Sigma_true.i();
		arma::mat B_init = arma::ones<arma::mat>(B_true.n_rows, B_true.n_cols) / 10.0;
		arma::mat Omega_init = random_Omega(Omega_true.n_rows, 
				Omega_true.n_cols);

		/* read command line arguments */
		if (argc != 6) {
			std::cout << "The number of arguments is not correct!" << std::endl;
			exit(1);
		}
		double epsilon = atof(argv[1]), gamma = atof(argv[2]), 
			   tau = atof(argv[3]);
		uint N = atoi(argv[4]);
		uint maxiter = atoi(argv[5]);

		double_vec lambda1_vec, lambda2_vec;
		double step = 0.2, lam_start = -2.8;
		for (uint i = 0; i < 10; ++i) {
			double lam_value = pow(10, lam_start + step * i);
			lambda1_vec.push_back(lam_value);
			lambda2_vec.push_back(lam_value);
		}

		/* perform the model selelction */
		model_param best_model = model_selection(X, Y, B_init, Omega_init, 
				lambda1_vec, lambda2_vec, epsilon, tau, gamma, N, fact_table, 
				"rf-mean", "N", maxiter);
		
		std::cout << "The best model: " << std::endl << "B matrix: " 
			<< std::endl << best_model.B << std::endl << "Omega matrix: " 
			<< std::endl << best_model.Omega << std::endl << "status: "
			<< best_model.status << std::endl << "number of EM iterations: "
			<< best_model.niters << std::endl << "eBIC score: " 
			<< best_model.eBIC << std::endl;

		arma::mat Y_est = predict_mvpln(X_test, best_model.B);
		arma::rowvec mse = arma::sum(arma::pow(Y_test - Y_est, 2), 0) / 
			double(Y_test.n_rows);
		arma::rowvec rmse = arma::sqrt(mse);

		/* compute the accumulative non-zero entries across different
		 * replications of run */
		B_entry_accuracy("./exp_results/B_est_accu_mvpln.csv", best_model.B);
		Omega_entry_accuracy("./exp_results/Omega_est_accu_mvpln.csv", 
				best_model.Omega);

		/* check the convergence status */
		if (best_model.converged){
			std::cout << "entering converge number block." << std::endl;
			converge_status_addone("./exp_results/converge_number.csv");
		}

		/* write the rmse to file */
		std::ofstream rmse_file;
		rmse_file.open("./exp_results/rmse_parallel.csv", 
				std::ios::out | std::ios::app);
		rmse.save(rmse_file, arma::csv_ascii);
		rmse_file.close();

		/* compute the f1 score of B matrix, erros of estimated B and Omega
		 * matrices, and write the results into file */
		double f1_score = compute_f1_score(B_true, best_model.B);
		std::pair<double, double> false_pos_neg = compute_false_pos_neg_rate(
				B_true, best_model.B);
		double B_dist = matrix_distance(best_model.B, B_true, "nf-norm");
		double Omega_dist = matrix_distance(best_model.Omega, Omega_true, 
				"nf-norm");
		std::ofstream f1_dist_file;
		f1_dist_file.open("./exp_results/f1_dist_parallel.csv", 
				std::ios::out | std::ios::app);
		f1_dist_file << f1_score << "," << false_pos_neg.first << "," << 
			false_pos_neg.second << "," << B_dist << "," << Omega_dist 
			<< std::endl;
		f1_dist_file.close();
	}
#endif
	return 0;
}
