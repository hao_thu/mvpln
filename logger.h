/*
  file: logger.h
  description: define the logger class using boost.log library
  author: HW
 */

#ifndef LOGGER_H
#define LOGGER_H

#define BOOST_LOG_DYN_LINK 1

#define PTR_NULL NULL

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>

#include <string>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

class Logger {
public:
	static Logger* getInstance(std::string logFile = "mvpln_cpp.log");
	void write_log (std::string message, std::string level = "info");

	void logInfo(std::string message);
	void logDebug(std::string message);
	void logWarn(std::string message);
	void logError(std::string message);
	void logFatal(std::string message);

private:
	Logger (){}
	virtual ~Logger (){}

private:
	/* data member */
	src::severity_logger< logging::trivial::severity_level > log_;
	static Logger* logger_;
};

#endif /* LOGGER_H */
